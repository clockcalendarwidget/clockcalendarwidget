# -------------------------------------------------
# Project created by QtCreator 2010-05-30T20:08:45
# -------------------------------------------------
VERSION = 0.2
DEFINES += APP_VERSION=$$VERSION
RC_FILE = clockcalendar.rc
TARGET = ClockCalendar
TEMPLATE = app
SOURCES += main.cpp \
    clockwithcalendarwidget.cpp \
    clockwidget.cpp \
    clockwithcalendarapp.cpp \
    config.cpp \
    aboutwidget.cpp \
    configdialog.cpp
HEADERS += clockwithcalendarwidget.h \
    clockwidget.h \
    clockwithcalendarapp.h \
    config.h \
    aboutwidget.h \
    configdialog.h
FORMS += clockwithcalendarwidget.ui \
    aboutwidget.ui \
    configdialog.ui

RESOURCES += \
    clockwithcalendar.qrc
