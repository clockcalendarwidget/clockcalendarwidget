#ifndef CLOCKWITHCALENDARWIDGET_H
#define CLOCKWITHCALENDARWIDGET_H

#include <QWidget>

namespace Ui {
    class ClockWithCalendarWidget;
}

class ClockWithCalendarWidget : public QWidget {
    Q_OBJECT
public:
    ClockWithCalendarWidget(QWidget *parent = 0);
    ~ClockWithCalendarWidget();

public slots:
    void updateTimeLabel();
    void onRightClick(QPoint);
    void resetDateToToday();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::ClockWithCalendarWidget *ui;
};

#endif // CLOCKWITHCALENDARWIDGET_H
