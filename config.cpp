#include "config.h"

#include <QSettings>
#include <QtDebug>
#include <QDir>
#include <QCoreApplication>

Config* config;

Config::Config()
{
        read();
}

Config::~Config()
{
        write();
}

void Config::read()
{
        qDebug() << "Config::read()";
        QSettings settings;

        windowGeom = settings.value("general/windowGeometry").toByteArray();
        resetDateOnPopup = settings.value("general/resetDateOnPopup", true).toBool();
}

void Config::write()
{
        qDebug() << "Config::write()";

        QSettings settings;
        settings.setValue("general/version", QCoreApplication::applicationVersion());
        settings.setValue("general/windowGeometry", windowGeom);
        settings.setValue("general/resetDateOnPopup", resetDateOnPopup);
}
