#include "clockwithcalendarapp.h"

#include "config.h"
#include "clockwithcalendarwidget.h"
#include "aboutwidget.h"
#include "configdialog.h"

#include <QtGui>

ClockWithCalendarApp::ClockWithCalendarApp(QObject* parent)
    : QObject(parent), aboutWidget(0), configDialog(0)
{
    config = new Config();
    initGUI();
}

ClockWithCalendarApp::~ClockWithCalendarApp()
{
    config->windowGeom = calendarWidget->saveGeometry();
    delete config;
    if (aboutWidget != 0) delete aboutWidget;
}

void ClockWithCalendarApp::initGUI()
{
    QIcon icon = QIcon(":/images/calendar.svg");

    QSystemTrayIcon* systrayIcon = new QSystemTrayIcon(icon, this);
    systrayIcon->setToolTip("Clock and calendar");
    connect(systrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(onSystrayIcon_clicked(QSystemTrayIcon::ActivationReason)));
    QMenu* contextMenu = new QMenu();
    QAction* action;
    action = contextMenu->addAction("&Toggle visibility", this, SLOT(toggleVisibility()));
    contextMenu->setDefaultAction(action);
    contextMenu->addAction("&Config", this, SLOT(showConfigDialog()));
    contextMenu->addAction("&About", this, SLOT(showAbout()));
    contextMenu->addAction("&Quit", QApplication::instance(), SLOT(quit()));
    systrayIcon->setContextMenu(contextMenu);

    systrayIcon->show();

    calendarWidget = new ClockWithCalendarWidget();
    calendarWidget->restoreGeometry(config->windowGeom);
}

void ClockWithCalendarApp::onSystrayIcon_clicked(QSystemTrayIcon::ActivationReason reason)
{
    if (reason == QSystemTrayIcon::Trigger) {
        toggleVisibility();
    }
}

void ClockWithCalendarApp::toggleVisibility()
{
    if (config->resetDateOnPopup) calendarWidget->resetDateToToday();
    calendarWidget->setVisible(!calendarWidget->isVisible());
}

void ClockWithCalendarApp::showAbout()
{
    if (aboutWidget == 0) {
        aboutWidget = new AboutWidget();
        aboutWidget->setVersion(qApp->applicationVersion());
        QRect desktopRect = QApplication::desktop()->screenGeometry(QCursor::pos());
//        aboutWidget->adjustSize();
        aboutWidget->move(desktopRect.center() - aboutWidget->rect().center());
    }
    aboutWidget->show();
}

void ClockWithCalendarApp::showConfigDialog()
{
    if (configDialog == 0) {
        configDialog = new ConfigDialog();
    }
    configDialog->resetGUI();
    configDialog->show();
}
