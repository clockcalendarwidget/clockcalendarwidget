#ifndef CLOCKWITHCALENDARAPP_H
#define CLOCKWITHCALENDARAPP_H


#include <QtCore>
#include <QtGui>

class ClockWithCalendarWidget;
class AboutWidget;
class ConfigDialog;

class ClockWithCalendarApp
    : public QObject
{
    Q_OBJECT

public:
    ClockWithCalendarApp(QObject* parent = NULL);
    ~ClockWithCalendarApp();

public slots:
    void onSystrayIcon_clicked(QSystemTrayIcon::ActivationReason);
    void toggleVisibility();
    void showAbout();
    void showConfigDialog();

private:
    void initGUI();

    ClockWithCalendarWidget* calendarWidget;
    AboutWidget* aboutWidget;
    ConfigDialog* configDialog;
};

#endif // CLOCKWITHCALENDARAPP_H
