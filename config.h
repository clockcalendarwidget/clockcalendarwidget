#ifndef CONFIG_H
#define CONFIG_H

#include <QString>
#include <QByteArray>

class Config
{
public:
    Config();
    virtual ~Config();

private:
    void read();
    void write();

public: // members
    QByteArray windowGeom;
    bool resetDateOnPopup;
};

extern Config* config;

#endif // CONFIG_H
