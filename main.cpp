#include <QtGui/QApplication>
#include "clockwithcalendarwidget.h"
#include "clockwithcalendarapp.h"

#define QUOTE_(x) #x
#define QUOTE(x) QUOTE_(x)

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("Mattiesworld");
    QCoreApplication::setOrganizationDomain("mattiesworld.gotdns.org");
    QCoreApplication::setApplicationName("ClockCalendarWidget");
    QCoreApplication::setApplicationVersion(QUOTE(APP_VERSION));

    QApplication app(argc, argv);
    app.setQuitOnLastWindowClosed(false);
    ClockWithCalendarApp clockWithCalendarApp(&app);
    return app.exec();
}
