#include "clockwithcalendarwidget.h"
#include "ui_clockwithcalendarwidget.h"

#include <QtCore/QTimer>
#include <QtCore/QDate>

ClockWithCalendarWidget::ClockWithCalendarWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ClockWithCalendarWidget)
{
    ui->setupUi(this);
    ui->calendarWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->calendarWidget, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(onRightClick(QPoint)));
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateTimeLabel()));
    timer->start(1000);
}

ClockWithCalendarWidget::~ClockWithCalendarWidget()
{
    delete ui;
}

void ClockWithCalendarWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void ClockWithCalendarWidget::updateTimeLabel()
{
    ui->timeLabel->setText(QTime::currentTime().toString());
}

void ClockWithCalendarWidget::resetDateToToday()
{
    ui->calendarWidget->setSelectedDate(QDate::currentDate());
}

void ClockWithCalendarWidget::onRightClick(QPoint)
{
    resetDateToToday();
}
