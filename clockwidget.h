#ifndef CLOCKWIDGET_H
#define CLOCKWIDGET_H

#include <QWidget>

 class ClockWidget : public QWidget
 {
     Q_OBJECT

 public:
     ClockWidget(QWidget *parent = 0);

 protected:
     void paintEvent(QPaintEvent *event);
 };

#endif // CLOCKWIDGET_H
