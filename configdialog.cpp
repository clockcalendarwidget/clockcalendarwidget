#include "configdialog.h"
#include "ui_configdialog.h"

#include "config.h"

#include <QtCore/QSettings>
#include <QtGui>

ConfigDialog::ConfigDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfigDialog)
{
    ui->setupUi(this);
    initGUI();
}

ConfigDialog::~ConfigDialog()
{
    delete ui;
}

void ConfigDialog::initGUI()
{
    adjustSize();
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(onAccept()));
}

void ConfigDialog::resetGUI()
{
    QSettings settings("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);
    ui->autoStartCB->setChecked(settings.value("ClockCalendar").toBool());

    ui->resetDateCB->setChecked(config->resetDateOnPopup);
}

void ConfigDialog::onAccept()
{
    QSettings settings("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);

    if (ui->autoStartCB->isChecked()) {
        settings.setValue("ClockCalendar", QCoreApplication::applicationFilePath());
    } else {
        settings.remove("ClockCalendar");
    }

    config->resetDateOnPopup = ui->resetDateCB->isChecked();
}
